#include <iostream>
using namespace std;

// function declaration
void bar();

// function definition
void foo() {
    bar();
}

// function definition
void bar() {
    foo();
}

int main() {

    /*
        - Create a void function called `foo`, which calls another function called `bar`

        - Create a void function called `bar`, which calls the function called `foo`
    */
    
    return 0;
}