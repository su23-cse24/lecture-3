#include <iostream>
using namespace std;

int increment(int x, int inc = 1) {
    return x + inc;
}

int sum(int x = 1, int y = 1) {
    return x + y;
}

int main() {

    /*
        - Create a function called `increment`, which returns an integer incremented by some constant (inc)
            - Takes two parameters (inc must have a default value)

        - Create a function called `sum`, which returns the sum of two integers
            - Takes two parameters (both with default values)
    */
    
    // calling with 2 paramaters
    cout << increment(5, 7) << endl;

    // calling with 1 paramaters
    cout << increment(5) << endl;

    // calling with 0 paramaters
    cout << sum() << endl;

    // calling with 1 paramaters
    cout << sum(3) << endl;

    // calling with 2 paramaters
    cout << sum(2, 3) << endl;

    return 0;
}