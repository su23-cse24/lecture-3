#include <iostream>
#include "ucm_strings.h"

using namespace std;

int main() {

    /*
        Tasks:
        Create a command-line menu driven app to do some simple word processing functions

        ===========================
         UC Merced Word Processing
        ===========================

        1. Count Vowels
        2. Count Words
        3. Convert to Upper Case
        4. Exit

        Select option: 
    */

    printMainMenu();

    int option;

    while(cin >> option) {
        if (option == 4) {
            break;
        }

        if (option >= 1 && option <= 3) {
            handleOptions(option);
        }

        printMainMenu();
    }

    cout << "Bye..." << endl;

    return 0;
}