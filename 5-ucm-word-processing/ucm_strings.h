#ifndef UCM_STRINGS_H
#define UCM_STRINGS_H

#include <iostream>

void printMainMenu() {
    system("clear");
    std::cout << "===========================" << std::endl;
    std::cout << " UC Merced Word Processing " << std::endl;
    std::cout << "===========================" << std::endl;
    std::cout << std::endl;
    std::cout << "1. Count Vowels" << std::endl;
    std::cout << "2. Count Words" << std::endl;
    std::cout << "3. Convert to Upper Case" << std::endl;
    std::cout << "4. Exit" << std::endl;
    std::cout << std::endl;
    std::cout << "Select an option: ";
}

int countVowels(std::string s) {
    int count = 0;
    for (int i = 0; i < s.length(); i++) {
        s[i] = toupper(s[i]);
        if (s[i] == 'A' || s[i] == 'E' || s[i] == 'I' || s[i] == 'O' || s[i] == 'U') {
            count++;
        }
    }
    return count;
}

int countWords(std::string s) {
    int count = 0;

    if (s.length() == 0) {
        return 0;
    }

    for (int i = 0; i < s.length(); i++) {
        if (s[i] == ' ') {
            count++;
        }
    }
    return count + 1;
}

void convertToUpper(std::string& s) {
    for (int i = 0; i < s.length(); i++) {
        s[i] = toupper(s[i]);
    }
}

std::string getUserInput() {
    std::string stringInput;
    std::cin.ignore();
    std::cout << "Enter a string: ";
    getline(std::cin, stringInput);
    return stringInput;
}

void waitForMainMenu() {
    std::cout << std::endl;
    std::cout << "Press [enter] to return to main menu";
    std::string dummy;
    getline(std::cin, dummy);
}

void handleOptions(int option) {
    system("clear");
    std::string stringInput = getUserInput();

    if (option == 1) {
        int vowels = countVowels(stringInput);
        std::cout << vowels << " vowels" << std::endl;
    } else if (option == 2) {
        int words = countWords(stringInput);
        std::cout << words << " words" << std::endl;
    } else if (option == 3) {
        convertToUpper(stringInput);
        std::cout << stringInput << std::endl;
    }

    waitForMainMenu();
}

#endif