#include <iostream>
using namespace std;

void printHelloWorld() {
    cout << "Hello World" << endl;
}

void printString (string str) {
    cout << str << endl;
}

int multiply(int a, int b) {
    return a * b;
}

int main() {

    /*
        - Create a void function called `printHelloWorld` which prints the string "Hello World"

        - Create a void function called `printString`, which takes in a string and prints it out

        - Create a function called `multiply`, which takes in two integers and returns their product
    */

    // printHelloWorld();

    // printString("CSE 24: Advanced Programming");
    // printString("Summer 2023");

    int c = multiply(3, 4);
    cout << c << endl;

    return 0;
}