#include <iostream>
using namespace std;

void incrementByValue(int x) {
    x = x + 1;
    cout << "x: " << (long) &x << " : " << x << endl;
}

void incrementByReference(int& x) {
    x = x + 1;
    cout << "x: " << (long) &x << " : " << x << endl;
}

int main() {

    /*
        - Create a void function called `incrementByValue` which takes in an integer as a parameter and increments it by 1
        
        - Create a void function called `incrementByReference` which takes in an reference to an integer as a parameter and increments it by 1
    */

    int number = 0;
    cout << "number: " << (long) &number << " : " << number << endl;
    incrementByReference(number);
    
    return 0;
}